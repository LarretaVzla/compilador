#include <stdio.h> //Para operaciones de Entrada y Salida (printf/scanf)
#include <stdlib.h> //Para conversión de tipos de datos
#include <string.h> //Operaciones de cadenas
#include <ctype.h> //Operaciones básicas con caracteres (isdigit(), isalpha(), isspace())
#include <stdbool.h> //Usar booleanos
FILE *archivo;
char caract[2];
char dato[100];
int aux;
char arr_token[500][2][100];
void condicion_tokens();
bool Prueba(char [10]);
bool Vali_nom_var();
bool Vali_text();
bool Vali_num_double();
    //Creacion de las "palabras reservadas" 
char _espa[10] = " ";
char _salto[10] = "\n";
char _asignacion[10] = "=";
char _fin[10] = ";";
char _ini_pare[10] = "(";
char _fin_pare[10] = ")";
char _ini_llave[10] = "{";
char _fin_llave[10] = "}";
char _comilla[10] = "\"";
char _negacion[10] = "!";
char _menorque[10] = "<";
char _mayorque[10] = ">";
char _and[10] = "&";
char _or[10] = "|";
char _suma[10] = "+";
char _resta[10] = "-";
char _verdadero[10] = "true";
char _falso[10] = "false";
char _tabu[10] = "\t";
char _break[10] = "break";
char _while[10] = "while";
char _double[10] = "double";
char _if[10] = "if";
char _else[10] = "else";
char _for[10] = "for";
char _int[10] = "int";
char _return[10] = "return";
char _string[10] = "string";
char _printf[10] = "printf";
char _scanf[10] = "scanf";
char _boolean[10] = "boolean";

int main(int argc, char const *argv[])
{
    archivo = fopen("Codigo_ejemplo.txt","r");
    if(archivo == NULL){
        printf("\nError al abrir el archivo.  :c\n\n");
    } else{
        condicion_tokens();
    }
    fclose(archivo);
    system("pause");
    return 0;
}
//analisis lexico
void condicion_tokens(){
    aux = ftell(archivo);
    int i=0;
    while (!feof(archivo)){
        if (Prueba(_break)){
            strcpy(arr_token[i][0], _break);
            //guardar token y proseguir
        }
        else if (Prueba(_while)){
            strcpy(arr_token[i][0], _while);
            //guardar token y proseguir
        }
        else if (Prueba(_double)){
            strcpy(arr_token[i][0], _double);
            //guardar token y proseguir
        }
        else if (Prueba(_if)){
            strcpy(arr_token[i][0], _if);
            //guardar token y proseguir
        }
        else if (Prueba(_else)){
            strcpy(arr_token[i][0], _else);
            //guardar token y proseguir
        }
        else if (Prueba(_for)){
            strcpy(arr_token[i][0], _for);
            //guardar token y proseguir
        }
        else if (Prueba(_int)){
            strcpy(arr_token[i][0], _int);
            //guardar token y proseguir
        }
        else if (Prueba(_return)){
            strcpy(arr_token[i][0], _return);
            //guardar token y proseguir
        }
        else if (Prueba(_string)){
            strcpy(arr_token[i][0], _string);
            //guardar token y proseguir
        }
        else if (Prueba(_printf)){
            strcpy(arr_token[i][0], _printf);
            //guardar token y proseguir
        }
        else if (Prueba(_scanf)){
            strcpy(arr_token[i][0], _scanf);
            //guardar token y proseguir
        }
        else if (Prueba(_boolean)){
            strcpy(arr_token[i][0], _boolean);
            //guardar token y proseguir
        }
        else if (Prueba(_salto)){
            strcpy(arr_token[i][0], "salto");
            //guardar token y proseguir
        }
        else if (Prueba(_ini_llave)){
            strcpy(arr_token[i][0], _ini_llave);
            //guardar token y proseguir
        }
        else if (Prueba(_fin_llave)){
            strcpy(arr_token[i][0], _fin_llave);
            //guardar token y proseguir
        }
        else if (Prueba(_espa)){
            strcpy(arr_token[i][0], "espacio");
            //guardar token y proseguir
        }
        else if (Prueba(_asignacion)){
            strcpy(arr_token[i][0], _asignacion);
            //guardar token y proseguir
        }
        else if (Prueba(_fin)){
            strcpy(arr_token[i][0], _fin);
            //guardar token y proseguir
        }
        else if (Prueba(_ini_pare)){
            strcpy(arr_token[i][0], _ini_pare);
            //guardar token y proseguir
        }
        else if (Prueba(_fin_pare)){
            strcpy(arr_token[i][0], _fin_pare);
            //guardar token y proseguir
        }
        else if (Prueba(_comilla)){
            strcpy(arr_token[i][0], "comilla");
            //guardar token y proseguir
        }
        else if (Prueba(_negacion)){
            strcpy(arr_token[i][0], _negacion);
            //guardar token y proseguir
        }
        else if (Prueba(_menorque)){
            strcpy(arr_token[i][0], _menorque);
            //guardar token y proseguir
        }
        else if (Prueba(_mayorque)){
            strcpy(arr_token[i][0], _mayorque);
            //guardar token y proseguir
        }
        else if (Prueba(_and)){
            strcpy(arr_token[i][0], _and);
            //guardar token y proseguir
        }
        else if (Prueba(_or)){
            strcpy(arr_token[i][0], _or);
            //guardar token y proseguir
        }
        else if (Prueba(_suma)){
            strcpy(arr_token[i][0], _suma);
            //guardar token y proseguir
        }
        else if (Prueba(_resta)){
            strcpy(arr_token[i][0], _resta);
            //guardar token y proseguir
        }
        else if (Prueba(_verdadero)){
            strcpy(arr_token[i][0], _verdadero);
            //guardar token y proseguir
        }
        else if (Prueba(_falso)){
            strcpy(arr_token[i][0], _falso);
            //guardar token y proseguir
        }
        else if (Prueba(_tabu)){
            strcpy(arr_token[i][0], "tabu");
            //guardar token y proseguir
        }
        else if (Vali_text()){
            strcpy(arr_token[i][0], "texto");
            //guardar token y proseguir
        }
        else if (Vali_nom_var()){
            strcpy(arr_token[i][0], "nombre");
            //guardar token y proseguir
        }
        else if (Vali_num_double()){
            strcpy(arr_token[i][0], "numero");
            //guardar token y proseguir
        }
        else{
             printf("token no valido\n");
             break;
        }
            strcpy(arr_token[i][1], dato);
            memset(dato,0,100);
            i++;
    }
    //arr_token[i][0] != '\0' no funciona esta condicion
    for (int i = 0; arr_token[i][0][0] != '\0'; i++)
    {
        printf("%s",arr_token[i][0]);
        printf(": %s\n",arr_token[i][1]);
        //strcmp(arr_token[i][0],"nombre") regresa 0 cuando son iguales
        getchar();
    }
}
//analiza caracter por caracter si el token es valido
bool Prueba(char dat[10]){
    for (int i=0; (!feof(archivo)); i++ ){
        if(dat[i]=='\0'){
            strcpy(dato, dat);
            aux = ftell(archivo);
            return true;
        }
        fgets(caract, 2, archivo);
        if (caract[0] != dat[i]){
            fseek(archivo, aux, SEEK_SET);
            return false;
        }
        //fseek(archivo, -1, SEEK_CUR);
    }
    return false;
}
//para validar si es el nombre de una variable
bool Vali_nom_var(){
    fgets(caract, 2, archivo);
    if (!isalpha(caract[0])){
        fseek(archivo, aux, SEEK_SET);
        return false;
    }
    else{
        fseek(archivo, aux, SEEK_SET);
    }
    for (int i = 0; (!feof(archivo)); i++)
    {
        fgets(caract, 2, archivo);
        if (!isalnum(caract[0]) | caract[0] == 95){
            aux = ftell(archivo) - 1;
            fseek(archivo, aux, SEEK_SET);
            return true;
        }else{
            //guardar letra
            dato[i] = caract[0];
        }
    }
    //si se termina el archivo sin encontrar un espacio, punto y coma o =
    fseek(archivo, aux, SEEK_SET);
    return false;
}
//para validar si se trata de un texto
bool Vali_text(){
    fseek(archivo, aux-1, SEEK_SET);
    fgets(caract, 2, archivo);
    if (caract[0] != _comilla[0]){
        fseek(archivo, aux, SEEK_SET);
        return false;
    }
    for (int i = 0; (!feof(archivo)); i++)
    {
        fgets(caract, 2, archivo);
        if (caract[0] == _comilla[0]){
            aux = ftell(archivo) - 1;
            fseek(archivo, aux, SEEK_SET);
            return true;
        }
        //ir guardando token de texto
        dato[i] = caract[0];
    }
    return false;
}
//validar si se trata de un numero double
bool Vali_num_double(){
    int pun = 0;
    fgets(caract, 2, archivo);
    if (isdigit(caract[0])){
        //guardar numero
        dato[0] = caract[0];
        for (int i = 0; (!feof(archivo)); i++)
        {
            if (pun > 1)
            {
                fseek(archivo, aux, SEEK_SET);
                return false;
            }
            fgets(caract, 2, archivo);
            if (caract[0] == _fin[0] | caract[0] == _fin_pare[0]){
                aux = ftell(archivo) - 1;
                fseek(archivo, aux, SEEK_SET);
                return true;
            }else{
                if (isdigit(caract[0])){
                    //guardar numero
                    dato[i+1] = caract[0];
                }
                else if(caract[0] == 46){
                    dato[i+1] = caract[0];
                    pun++;
                }else{
                    fseek(archivo, aux, SEEK_SET);
                    return false;
                }
            }
        }
    }else{
        fseek(archivo, aux, SEEK_SET);
        return false;
    }
}