#include <stdio.h> //Para operaciones de Entrada y Salida (printf/scanf)
#include <stdlib.h> //Para conversión de tipos de datos
#include <string.h> //Operaciones de cadenas
#include <ctype.h> //Operaciones básicas con caracteres (isdigit(), isalpha(), isspace())
#include <stdbool.h>
FILE *archivo;
char caract[2];
char dato[100];
int aux;
int puntero_sintac = 0;
int llaves = 0;
char arr_token[500][2][100];
struct Palabras{
    //Creacion de estructura donde estaran las "palabras reservadas" 
    char _espa[10];
    char _salto[10];
    char _asignacion[10];
    char _fin[10];
    char _ini_pare[10];
    char _fin_pare[10];
    char _ini_llave[10];
    char _fin_llave[10];
    char _comilla[10];
    char _negacion[10];
    char _menorque[10];
    char _mayorque[10];
    char _and[10];
    char _or[10];
    char _suma[10];
    char _resta[10];
    char _verdadero[10];
    char _falso[10];
    char _tabu[10];
    char _break[10];
    char _while[10];
    char _double[10];
    char _if[10];
    char _else[10];
    char _for[10];
    char _int[10];
    char _return[10];
    char _string[10];
    char _printf[10];
    char _scanf[10];
    char _boolean[10];
};

struct Palabras inpalabras(){
    //Cargar las palabras que usaremos en cada variable de la estructura y regresar una variable de ese tipo
    struct Palabras pala;
    strcpy(pala._espa, " ");//          
    strcpy(pala._salto, "\n");//
    strcpy(pala._asignacion, "=");//
    strcpy(pala._fin, ";");//
    strcpy(pala._ini_pare, "(");//
    strcpy(pala._fin_pare, ")");//
    strcpy(pala._ini_llave, "{");//
    strcpy(pala._fin_llave, "}");//
    strcpy(pala._comilla, "\"");//
    strcpy(pala._negacion, "!");//
    strcpy(pala._menorque, "<");//
    strcpy(pala._mayorque, ">");//
    strcpy(pala._and, "&");//
    strcpy(pala._or, "|");//
    strcpy(pala._suma, "+");//
    strcpy(pala._resta, "-");//
    strcpy(pala._verdadero, "true");//
    strcpy(pala._falso, "false");//
    strcpy(pala._tabu, "\t");//
    strcpy(pala._break, "break");//
    strcpy(pala._while, "while");//
    strcpy(pala._double, "double");//
    strcpy(pala._if, "if");//
    strcpy(pala._else, "else");//
    strcpy(pala._for, "for");//
    strcpy(pala._int, "int");//
    strcpy(pala._return, "return");//
    strcpy(pala._string, "string");//
    strcpy(pala._printf, "printf");//
    strcpy(pala._scanf, "scanf");//
    strcpy(pala._boolean, "boolean");//
    return pala;
}
//regresa true en caso de encontrar a-zA-Z0-9 o _
bool letras(char letra){
    if ((letra > 64 & letra < 91) | (letra > 96 & letra < 123) | (letra > 47 & letra < 58) | (letra == 95)){
        return true;
    }
    return false;
}
//regresa true en caso de encontrar 0-9 o _
bool numeros(char numero){
    if ((numero > 47 & numero < 58) | (numero == 95) ){
        return true;
    }
    return false;
}
//validar si se trata de un numero double
bool Vali_num_double(struct Palabras palabras){
    
    printf("posicion del puntero: %i\n", ftell(archivo));
    fgets(caract, 2, archivo);
    if (numeros(caract[0]) & caract[0] != 95 ){
        //guardar numero
        dato[0] = caract[0];
        for (int i = 0; (!feof(archivo)); i++)
        {
            printf("posicion del puntero: %i\n", ftell(archivo));
            fgets(caract, 2, archivo);
            if (caract[0] == palabras._fin[0] | caract[0] == palabras._fin_pare[0]){
                printf("Token de numero double guardado\n");
                aux = ftell(archivo) - 1;
                fseek(archivo, aux, SEEK_SET);
                return true;
            }else{
                if (numeros(caract[0]) | caract[0] == 46 & caract[0] != 95 ){
                    printf("guardar numero\n");
                    //guardar numero
                    dato[i+1] = caract[0];
                }else{
                    printf("token no valido\n");
                    fseek(archivo, aux, SEEK_SET);
                    return false;
                }
            }
            
        }
            
    }else{
        printf("token no valido\n");
        fseek(archivo, aux, SEEK_SET);
        return false;
    }
    
    
}
//para validar si se trata de un texto
bool Vali_text(struct Palabras palabras){
    fseek(archivo, aux-1, SEEK_SET);
    fgets(caract, 2, archivo);
    if (caract[0] != palabras._comilla[0]){
        fseek(archivo, aux, SEEK_SET);
        return false;
    }
    for (int i = 0; (!feof(archivo)); i++)
    {
        printf("posicion del puntero: %i\n", ftell(archivo));
        fgets(caract, 2, archivo);
        if (caract[0] == palabras._comilla[0]){
            printf("token de texto guardado\n");
            aux = ftell(archivo) - 1;
            fseek(archivo, aux, SEEK_SET);
            return true;
        }
        //ir guardando token de texto
        dato[i] = caract[0];
    }
    
    return false;
}
//para validar si es el nombre de una variable
bool Vali_nom_var(struct Palabras palabras){
    printf("posicion del puntero: %i\n", ftell(archivo));
    fgets(caract, 2, archivo);
    if (numeros(caract[0])){
        printf("token no valido\n");
        fseek(archivo, aux, SEEK_SET);
        return false;
    }
    else{
        fseek(archivo, aux, SEEK_SET);
    }
    for (int i = 0; (!feof(archivo)); i++)
    {
        printf("posicion del puntero: %i\n", ftell(archivo));
        fgets(caract, 2, archivo);
        if (caract[0] == palabras._espa[0]  | caract[0] == palabras._asignacion[0] | caract[0] == palabras._fin[0] | caract[0] == palabras._fin_pare[0] | caract[0] == palabras._menorque[0] | caract[0] == palabras._mayorque[0] | caract[0] == palabras._menorque[0] | caract[0] == palabras._suma[0] | caract[0] == palabras._resta[0] | caract[0] == palabras._negacion[0]){
            printf("Token de nombre de variable guardada\n");
            aux = ftell(archivo) - 1;
            fseek(archivo, aux, SEEK_SET);
            return true;
        }else {
            if (!letras(caract[0])){
                printf("token no valido\n");
                fseek(archivo, aux, SEEK_SET);
                return false;
            }else{
                //guardar letra
                dato[i] = caract[0];
            }
        }
    }
    //si se termina el archivo sin encontrar un espacio, punto y coma o =
    return false;
    
}

//lo mismo que prueba pero solamente para validar si se trata de una funcion
bool Vali_fun(struct Palabras palabras){
    printf("posicion del puntero: %i\n", ftell(archivo));
    fgets(caract, 2, archivo);
    if (numeros(caract[0])){
        printf("token no valido\n");
        fseek(archivo, aux, SEEK_SET);
        return false;
    }
    else{
        fseek(archivo, aux, SEEK_SET);
    }
    for (int i = 0; (!feof(archivo)); i++)
    {
        printf("posicion del puntero: %i\n", ftell(archivo));
        fgets(caract, 2, archivo);
        if (caract[0] == palabras._ini_pare[0]){
            printf("Token de nombre de variable guardada\n");
            aux = ftell(archivo) - 1;
            fseek(archivo, aux, SEEK_SET);
            return true;
        }else {
            if (!letras(caract[0])){
                printf("token no valido\n");
                fseek(archivo, aux, SEEK_SET);
                return false;
            }else{
                //guardar letra
                dato[i] = caract[0];
            }
        }
    }
    return false;
}
//analiza caracter por caracter si el token es valido
bool Prueba(char dat[10]){
    // while((!feof(archivo))){
    //     fgets(caract, 2, archivo);
    //     if (caract[0] == dato[0]){
    //         printf("encontre una d\n");
    //     }else {
    //         printf("no encontre una b\n");
    //     }
    //     fgets(caract, 2, archivo);
    //     if (caract[0] == dato[0]){
    //         printf("encontre una d\n");
    //     }else {
    //         printf("no encontre una b\n");
    //     }
    //     break;
    // }
    

    for (int i=0; (!feof(archivo)); i++ ){
        printf("posicion del puntero: %i\n", ftell(archivo));
        if(dato[i]=='\0'){
            aux = ftell(archivo);
            return true;
        }
        printf("%d", i);
        fgets(caract, 2, archivo);
        if (caract[0] == dato[i]){
            printf("letras iguales: %c\n", caract[0]);
        }else {
            printf("token no valido\n");
            fseek(archivo, aux, SEEK_SET);
            memset(dato,0,100);
            return false;
        }
        //fseek(archivo, -1, SEEK_CUR);
        
    }
    memset(dato,0,100);
    return false;
}
//analisis lexico
void condicion_tokens(){
    struct Palabras palabra;
    palabra = inpalabras();
    aux = ftell(archivo);
    int i=0;
    while (!feof(archivo)){
        //strcpy(dato, palabra._break);
        //printf("prueba de algo : %s\n",dato);
        if (Prueba(strcpy(dato, palabra._break))){
            printf("token valido\n");
            strcpy(arr_token[i][0], "break");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._while)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "while");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._double)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "double");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._if)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "if");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._else)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "else");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._for)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "for");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._int)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "int");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._return)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "return");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._string)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "string");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._printf)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "printf");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._scanf)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "scanf");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._boolean)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "boolean");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._salto)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "salto");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._ini_llave)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "{");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._fin_llave)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "}");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._espa)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "espacio");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._asignacion)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "=");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._fin)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], ";");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._ini_pare)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "(");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._fin_pare)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], ")");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._comilla)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "comilla");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._negacion)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "!");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._menorque)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "<");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._mayorque)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], ">");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._and)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "&");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._or)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "|");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._suma)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "+");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._resta)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "-");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._verdadero)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "true");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._falso)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "false");
            //guardar token y proseguir
        }
        else if (Prueba(strcpy(dato, palabra._tabu)))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "tabu");
            //guardar token y proseguir
        }
        else if (Vali_fun(palabra)){
            printf("token valido\n");
            strcpy(arr_token[i][0], "funcion");
            //guardar token y proseguir
        }
        else if (Vali_text(palabra)){
            printf("token valido\n");
            strcpy(arr_token[i][0], "texto");
            //guardar token y proseguir
        }
        else if (Vali_nom_var(palabra))
        {
            printf("token valido\n");
            strcpy(arr_token[i][0], "nombre");
            //guardar token y proseguir
        }
        else if (Vali_num_double(palabra)){
            printf("token valido\n");
            strcpy(arr_token[i][0], "numero");
            //guardar token y proseguir
        }
        else{
             printf("token no valido\n");
             break;
        }
            strcpy(arr_token[i][1], dato);
            memset(dato,0,100);
            i++;
    }
    //arr_token[i][0] != '\0' no funciona esta condicion
    for (int i = 0; arr_token[i][0][0] != '\0'; i++)
    {
        printf("%s",arr_token[i][0]);
        printf(": %s\n",arr_token[i][1]);
        //strcmp(arr_token[i][0],"nombre") regresa 0 cuando son iguales
        
    }
    
    
    
}
//true si es un token de tipo: int, string o double
bool tipo_token(int puntero){
    if (!strcmp(arr_token[puntero][0],"int") | !strcmp(arr_token[puntero][0],"double") | !strcmp(arr_token[puntero][0],"string"))
    {
        return true;
    }
    return false;
}
//true si es un token de operador relacional: <, >, ==, !=, <=, >=
bool operador_rela_token(int puntero){
    if (!strcmp(arr_token[puntero][0],"<") | !strcmp(arr_token[puntero][0],">"))
    {
        return true;
    }
    else if ((!strcmp(arr_token[puntero][0],"=") & !strcmp(arr_token[puntero+1][0],"=")) | (!strcmp(arr_token[puntero][0],"!") & !strcmp(arr_token[puntero+1][0],"=")) | (!strcmp(arr_token[puntero][0],"<") & !strcmp(arr_token[puntero+1][0],"=")) | (!strcmp(arr_token[puntero][0],">") & !strcmp(arr_token[puntero+1][0],"=")))
    {
        //se agrega +1 a puntero_sintac para poder llegar a la siguiente linea ya que aqui se analizan dos tokens en lugar de uno
        puntero_sintac +=1;
        return true;
    }
    
    return false;
}
//true si es un token de aumento: ++, --, +numero, -numero
bool aumento_token(int puntero){
if ((!strcmp(arr_token[puntero][0],"+") & !strcmp(arr_token[puntero+1][0],"+")) | (!strcmp(arr_token[puntero][0],"-") & !strcmp(arr_token[puntero+1][0],"-")) | (!strcmp(arr_token[puntero][0],"+") & !strcmp(arr_token[puntero+1][0],"numero")) | (!strcmp(arr_token[puntero][0],"-") & !strcmp(arr_token[puntero+1][0],"numero")))
    {
        return true;
    }
    return false;
}
//true si es un token de operador aritmetico: +, -, *, /...
bool operador_arit_token(int puntero){
    if (!strcmp(arr_token[puntero][0],"+") | !strcmp(arr_token[puntero][0],"-") | !strcmp(arr_token[puntero][0],"*") | !strcmp(arr_token[puntero][0],"/"))
    {
        return true;
    }
    return false;
}
//true si es un token bool: true, false
bool bool_token(int puntero){
    if (!strcmp(arr_token[puntero][0],"true") | !strcmp(arr_token[puntero][0],"false"))
    {
        return true;
    }
    return false;
}
//true si es alguna de las sig combinaciones: 
//numero operador_arit numero
//numero operador_arit nombre
//nombre operador_arit numero
//nombre operador_arit nombre
bool num_operador_var(int puntero){
    if ((!strcmp(arr_token[puntero][0],"numero") | !strcmp(arr_token[puntero][0],"nombre")) & (operador_arit_token(puntero+1)) & (!strcmp(arr_token[puntero+2][0],"numero") | !strcmp(arr_token[puntero+2][0],"nombre")))
    {
        return true;
    }
    return false;
}
//true si es un token numero o comilla texto comilla
bool numortext(int puntero){
    if (!strcmp(arr_token[puntero][0],"numero"))
    {
        return true;
    }else if (!strcmp(arr_token[puntero][0],"comilla") & !strcmp(arr_token[puntero+1][0],"texto") & !strcmp(arr_token[puntero+2][0],"comilla"))
    {
        puntero_sintac +=2;
        return true;
    }
    
    return false;
}

//analisis sintactico
bool analisis_sintactico(){
    int linea = 0;
    while (arr_token[puntero_sintac][0][0] != '\0')
    {
        //cada vez que un analisis es correcto se le suma cada token a puntero_sintac y tambien se le suma un salto de linea para que analise la siguiente linea de codigo
        linea++;
        //double espacio nombre;
        if (!strcmp(arr_token[puntero_sintac][0],"double") & !strcmp(arr_token[puntero_sintac+1][0],"espacio") & !strcmp(arr_token[puntero_sintac+2][0],"nombre") & !strcmp(arr_token[puntero_sintac+3][0],";")){
            puntero_sintac += 5;
        }
        //double espacio nombre = numero;
        else if(!strcmp(arr_token[puntero_sintac][0],"double") & !strcmp(arr_token[puntero_sintac+1][0],"espacio") & !strcmp(arr_token[puntero_sintac+2][0],"nombre") & !strcmp(arr_token[puntero_sintac+3][0],"=") & !strcmp(arr_token[puntero_sintac+4][0],"numero") & !strcmp(arr_token[puntero_sintac+5][0],";")){
            puntero_sintac += 7;
        }
        //int espacio nombre;
        else if (!strcmp(arr_token[puntero_sintac][0],"int") & !strcmp(arr_token[puntero_sintac+1][0],"espacio") & !strcmp(arr_token[puntero_sintac+2][0],"nombre") & !strcmp(arr_token[puntero_sintac+3][0],";")){
            puntero_sintac += 5;
        }
        //int espacio nombre = numero;
        else if(!strcmp(arr_token[puntero_sintac][0],"int") & !strcmp(arr_token[puntero_sintac+1][0],"espacio") & !strcmp(arr_token[puntero_sintac+2][0],"nombre") & !strcmp(arr_token[puntero_sintac+3][0],"=") & !strcmp(arr_token[puntero_sintac+4][0],"numero") & !strcmp(arr_token[puntero_sintac+5][0],";")){
            puntero_sintac += 7;
        }
        //string espacio nombre;
        else if (!strcmp(arr_token[puntero_sintac][0],"string") & !strcmp(arr_token[puntero_sintac+1][0],"espacio") & !strcmp(arr_token[puntero_sintac+2][0],"nombre") & !strcmp(arr_token[puntero_sintac+3][0],";")){
            puntero_sintac += 5;
        }
        //string espacio nombre = comilla texto comilla;
        else if(!strcmp(arr_token[puntero_sintac][0],"string") & !strcmp(arr_token[puntero_sintac+1][0],"espacio") & !strcmp(arr_token[puntero_sintac+2][0],"nombre") & !strcmp(arr_token[puntero_sintac+3][0],"=") & !strcmp(arr_token[puntero_sintac+4][0],"comilla") & !strcmp(arr_token[puntero_sintac+5][0],"texto") & !strcmp(arr_token[puntero_sintac+6][0],"comilla") & !strcmp(arr_token[puntero_sintac+7][0],";")){
            puntero_sintac += 9;
        }
        //funcion();
        else if (!strcmp(arr_token[puntero_sintac][0],"funcion") & !strcmp(arr_token[puntero_sintac+1][0],"(") & !strcmp(arr_token[puntero_sintac+2][0],")") & !strcmp(arr_token[puntero_sintac+3][0],";")){
            puntero_sintac += 5;
        }
        //scanf(nombre);
        else if (!strcmp(arr_token[puntero_sintac][0],"scanf") & !strcmp(arr_token[puntero_sintac+1][0],"(") & !strcmp(arr_token[puntero_sintac+2][0],"nombre") & !strcmp(arr_token[puntero_sintac+3][0],")") & !strcmp(arr_token[puntero_sintac+4][0],";")){
            puntero_sintac += 6;
        }
        //printf(comilla texto comilla + nombre);
        else if(!strcmp(arr_token[puntero_sintac][0],"printf") & !strcmp(arr_token[puntero_sintac+1][0],"(") & !strcmp(arr_token[puntero_sintac+2][0],"comilla") & !strcmp(arr_token[puntero_sintac+3][0],"texto") & !strcmp(arr_token[puntero_sintac+4][0],"comilla") & !strcmp(arr_token[puntero_sintac+5][0],"+") & !strcmp(arr_token[puntero_sintac+6][0],"nombre") & !strcmp(arr_token[puntero_sintac+7][0],")") & !strcmp(arr_token[puntero_sintac+8][0],";")){
            puntero_sintac += 10;
        }
        //printf(nombre);
        else if (!strcmp(arr_token[puntero_sintac][0],"printf") & !strcmp(arr_token[puntero_sintac+1][0],"(") & !strcmp(arr_token[puntero_sintac+2][0],"nombre") & !strcmp(arr_token[puntero_sintac+3][0],")") & !strcmp(arr_token[puntero_sintac+4][0],";")){
            puntero_sintac += 6;
        }
        //printf(nombre + comilla texto comilla);
        else if(!strcmp(arr_token[puntero_sintac][0],"printf") & !strcmp(arr_token[puntero_sintac+1][0],"(") & !strcmp(arr_token[puntero_sintac+2][0],"nombre") & !strcmp(arr_token[puntero_sintac+3][0],"+") & !strcmp(arr_token[puntero_sintac+4][0],"comilla") & !strcmp(arr_token[puntero_sintac+5][0],"texto") & !strcmp(arr_token[puntero_sintac+6][0],"comilla") & !strcmp(arr_token[puntero_sintac+7][0],")") & !strcmp(arr_token[puntero_sintac+8][0],";")){
            puntero_sintac += 10;
        }
        //printf(comilla texto comilla);
        else if(!strcmp(arr_token[puntero_sintac][0],"printf") & !strcmp(arr_token[puntero_sintac+1][0],"(") & !strcmp(arr_token[puntero_sintac+2][0],"comilla") & !strcmp(arr_token[puntero_sintac+3][0],"texto") & !strcmp(arr_token[puntero_sintac+4][0],"comilla") & !strcmp(arr_token[puntero_sintac+5][0],")") & !strcmp(arr_token[puntero_sintac+6][0],";")){
            puntero_sintac += 8;
        }
        //int espacio nombre = funcion();
        //double
        //string
        else if(tipo_token(puntero_sintac) & !strcmp(arr_token[puntero_sintac+1][0],"espacio") & !strcmp(arr_token[puntero_sintac+2][0],"nombre") & !strcmp(arr_token[puntero_sintac+3][0],"=") & !strcmp(arr_token[puntero_sintac+4][0],"funcion") & !strcmp(arr_token[puntero_sintac+5][0],"(") & !strcmp(arr_token[puntero_sintac+6][0],")") & !strcmp(arr_token[puntero_sintac+7][0],";")){
            puntero_sintac += 9;
        }
        //nombre = funcion();
        else if(!strcmp(arr_token[puntero_sintac][0],"nombre") & !strcmp(arr_token[puntero_sintac+1][0],"=") & !strcmp(arr_token[puntero_sintac+2][0],"funcion") & !strcmp(arr_token[puntero_sintac+3][0],"(") & !strcmp(arr_token[puntero_sintac+4][0],")") & !strcmp(arr_token[puntero_sintac+5][0],";")){
            puntero_sintac += 7;
        }
        //for(nombre = numero; nombre operador_rela numero; nombre aumento_token){
        else if(!strcmp(arr_token[puntero_sintac][0],"for") & !strcmp(arr_token[puntero_sintac+1][0],"(") & !strcmp(arr_token[puntero_sintac+2][0],"nombre") & !strcmp(arr_token[puntero_sintac+3][0],"=") & !strcmp(arr_token[puntero_sintac+4][0],"numero") & !strcmp(arr_token[puntero_sintac+5][0],";") & !strcmp(arr_token[puntero_sintac+6][0],"nombre") & operador_rela_token(puntero_sintac+7) & !strcmp(arr_token[puntero_sintac+8][0],"numero") & !strcmp(arr_token[puntero_sintac+9][0],";") & !strcmp(arr_token[puntero_sintac+10][0],"nombre") & aumento_token(puntero_sintac+11) & !strcmp(arr_token[puntero_sintac+13][0],")") & !strcmp(arr_token[puntero_sintac+14][0],"{")){
            puntero_sintac += 16;
            //se suma a llaves 1 cada vez que hay una { o }
            llaves +=1;
        }
        //if (nombre operador_rela numortext) {
        else if(!strcmp(arr_token[puntero_sintac][0],"if") & !strcmp(arr_token[puntero_sintac+1][0],"(") & !strcmp(arr_token[puntero_sintac+2][0],"nombre") & operador_rela_token(puntero_sintac+3) & numortext(puntero_sintac+4) & !strcmp(arr_token[puntero_sintac+5][0],")") & !strcmp(arr_token[puntero_sintac+6][0],"{")){
            puntero_sintac += 8;
            //se suma a llaves 1 cada vez que hay una { o }
            llaves +=1;
        }
        //nombre = numortext;
        else if (!strcmp(arr_token[puntero_sintac][0],"nombre") & !strcmp(arr_token[puntero_sintac+1][0],"=") & numortext(puntero_sintac+2) & !strcmp(arr_token[puntero_sintac+3][0],";")){
            puntero_sintac += 5;
        }
        //nombre = num_operador_var;
        else if(!strcmp(arr_token[puntero_sintac][0],"nombre") & !strcmp(arr_token[puntero_sintac+1][0],"=") & num_operador_var(puntero_sintac+2) &!strcmp(arr_token[puntero_sintac+5][0],";")){
            puntero_sintac += 7;
        }
        //while (nombre operador_rela numortext) {
        else if(!strcmp(arr_token[puntero_sintac][0],"while") & !strcmp(arr_token[puntero_sintac+1][0],"(") & !strcmp(arr_token[puntero_sintac+2][0],"nombre") & operador_rela_token(puntero_sintac+3) & numortext(puntero_sintac+4) & !strcmp(arr_token[puntero_sintac+5][0],")") & !strcmp(arr_token[puntero_sintac+6][0],"{")){
            puntero_sintac += 8;
            //se suma a llaves 1 cada vez que hay una { o }
            llaves +=1;
        }
        //else {
        else if (!strcmp(arr_token[puntero_sintac][0],"else") & !strcmp(arr_token[puntero_sintac+1][0],"{")){
            puntero_sintac += 3;
            //se suma a llaves 1 cada vez que hay una { o }
            llaves +=1;
        }
        //else if
        //analizar si hay un else if, solo avanzar uno para que el if sea analizado aparte
        else if (!strcmp(arr_token[puntero_sintac][0],"else") & !strcmp(arr_token[puntero_sintac+1][0],"if")){
            puntero_sintac += 1;
        }
        //break;
        else if (!strcmp(arr_token[puntero_sintac][0],"break") & !strcmp(arr_token[puntero_sintac+1][0],";")){
            puntero_sintac += 3;
        }
        //funcion (){
        else if (!strcmp(arr_token[puntero_sintac][0],"funcion") & !strcmp(arr_token[puntero_sintac+1][0],"(") & !strcmp(arr_token[puntero_sintac+2][0],")") & !strcmp(arr_token[puntero_sintac+3][0],"{")){
            puntero_sintac += 5;
            llaves +=1;
        }
        //}
        else if (!strcmp(arr_token[puntero_sintac][0],"}")){
            puntero_sintac += 2;
            llaves +=1;
        }
        //while(true){
        //while(false){
        else if (!strcmp(arr_token[puntero_sintac][0],"while") & !strcmp(arr_token[puntero_sintac+1][0],"(") & (!strcmp(arr_token[puntero_sintac+2][0],"true") | !strcmp(arr_token[puntero_sintac+2][0],"false")) & !strcmp(arr_token[puntero_sintac+3][0],")") & !strcmp(arr_token[puntero_sintac+4][0],"{")){
            puntero_sintac += 6;
            llaves+=1;
        }
        //if(true){
        //if(false){
        else if (!strcmp(arr_token[puntero_sintac][0],"if") & !strcmp(arr_token[puntero_sintac+1][0],"(") & (!strcmp(arr_token[puntero_sintac+2][0],"true") | !strcmp(arr_token[puntero_sintac+2][0],"false")) & !strcmp(arr_token[puntero_sintac+3][0],")") & !strcmp(arr_token[puntero_sintac+4][0],"{")){
            puntero_sintac += 6;
            llaves+=1;
        }
        //return espacio numortext;
        //return espacio true;
        //return espacio false;
        else if (!strcmp(arr_token[puntero_sintac][0],"return") & !strcmp(arr_token[puntero_sintac+1][0],"espacio") & (!strcmp(arr_token[puntero_sintac+2][0],"true") | !strcmp(arr_token[puntero_sintac+2][0],"false") | numortext(puntero_sintac+2)) & !strcmp(arr_token[puntero_sintac+3][0],";")){
            puntero_sintac += 5;
        }else{
            printf("error en la linea: %i\n", linea);
            return false;
        }
        
    }
    return true;

}

int main(int argc, char const *argv[])
{
    archivo = fopen("Codigo_ejemplo.txt","r");
    if(archivo == NULL){
        printf("\nError al abrir el archivo.  :c\n\n");
    } else{
        condicion_tokens();
        if (analisis_sintactico())
        {
            if(llaves % 2 == 0){
            printf("todo correcto\n");
            //compilar codigo y ejecutar
            }
        }
    }
    fclose(archivo);

    system("pause");
    return 0;
}