#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <stdbool.h>

//Header y Footer del archivo
char header[] = "#include <stdio.h>\n#include <stdlib.h>\n#include <ctype.h>\n#include <string.h>\n#include <stdbool.h>\n\nint main(int argc, char const *argv[])\n{\n\t";
char footer[] = "\n\n\tprintf(\"\\n\");\n\tsystem(\"pause\");\n\treturn 0;\n}";
char **arr_variables;
char **arr_funciones;
FILE *archivo;
char caract[2];
char dato[100];
int ptr_limpio, ptr_token, aux;
char* arr_limpio;
char arr_tokens[500][2][100];

//Tokens :D
//Palabras reservadas: Tipo de token | Lenguaje C | Lenguaje propio
char t_int[3][4] = {"Tipo","int", "ent"};
char t_float[3][10] = {"Tipo","float", "decimal"};
char t_char[3][10] = {"Tipo","char[]", "string"};
char t_bool[3][10] = {"Tipo","bool", "bool"};
char t_const[3][10] = {"Tipo","const", "const"};
char t_printf[3][10] = {"E/S","printf", "show"};
char t_scanf[3][10] = {"E/S","scanf", "input"};
char t_if[3][10] = {"Condi","if", "if"};
char t_else[3][10] = {"Condi","else", "else"};
char t_for[3][10] = {"CicloF","for", "for"};
char t_while[3][10] = {"CicloW","while", "while"};
char t_true[3][10] = {"Booleano","true", "true"};
char t_false[3][10] = {"Booleano","false", "false"};
char t_break[3][10] = {"Break","break", "break"};
char t_return[3][10] = {"Regresar","return", "return"};
char t_include[3][10] = {"Libreria","#include", "require"};

//Signos de agrupación
char t_abre_pare[3][15] = {"Agrupa", "Abre Parent", "("};
char t_cierra_pare[3][15] = {"Agrupa", "Cierra Parent", ")"};
char t_abre_llave[3][15] = {"Agrupa", "Abre Llave", "{"};
char t_cierra_llave[3][15] = {"Agrupa", "Cierra Llave", "}"};
char t_abre_corche[3][15] = {"Agrupa", "Abre Corche", "["};
char t_cierra_corche[3][15] = {"Agrupa", "Cuerra Corche", "]"};

//Operadores aritméticos
char t_suma[3][10] = {"OpArit", "Suma", "+"};
char t_resta[3][10] = {"OpArit", "Resta", "-"};
char t_multi[3][10] = {"OpArit", "Multi", "*"};
char t_divi[3][10] = {"OpArit", "Division", "/"};

//Operadores relacionales
char t_menorque[3][10] = {"OpRela", "Menor que", "<"};
char t_mayorque[3][10] = {"OpRela", "Mayor que", ">"};
char t_asignacion[3][10] = {"OpRela", "Igual", "="};

//Operadores lógicos
char t_and[3][10] = {"OpLogico", "&&", "and"};
char t_or[3][10] = {"OpLogico", "||", "or"};
char t_nega[3][10] = {"OpLogico", "Negacion", "!"};

//Demás
char t_espacio[3][10] = {"Signo", "Espacio", " "};
char t_comillad[3][10] = {"Signo", "Comilla D", "\""};
char t_comillas[3][10] = {"Signo", "Comilla S", "\'"};
char t_salto[3][10] = {"Signo", "Salto","\n"};
char t_tabu[3][10] = {"Signo", "Tabu","\t"};
char t_pycoma[3][10] = {"Signo", "Pto y Coma",";"};


//Funcion que busca si el caracter actual o el ultimo caracter de arr_limpio es uno de estos signos
bool BuscarSignos(char actu, char ante){ //Caracter actual de arr_archivo y ultimo de arr_limpio
    char signos[] = {
        '(',')',
        '{','}',
        '[',']',
        ';','=',
        '+','-',
        '*','/',
        '<','>',
        ','
    };

    //Si encuentra una coincidencia regresa false
    for(int x = 0; x < strlen(signos); x++){
        if(actu == signos[x] || ante == signos[x]){
            return false;
        }
    }
    return true;
}

//Quitar comentarios, tabulaciones, espacios dobles y más
char* LimpiarCodigo(char* arr_archivo, char* arr_limpio){
    //Recorro todo el archivo guardado
    int ptr_arrlimpio = 0; //Puntero de arr_limpio

    for (int i = 0; i < strlen(arr_archivo); i++){//La i es el puntero del archivo guardado (arr_archivo)
        if (arr_archivo[i] == ' '){
            int esp = 1;
            
            while (true){
                i++;
                if (arr_archivo[i] != ' '){
                    //Si el caracter actual o el último de arr_limpio es uno de los signos que
                    //busca la función BuscarSignos entonces no pone el espacio
                    if (BuscarSignos(arr_archivo[i], arr_limpio[ptr_arrlimpio -1])){
                        if(esp > 0) {
                            if(arr_limpio[ptr_arrlimpio - 1] != ' '){
                                arr_limpio[ptr_arrlimpio] = ' ';
                                ptr_arrlimpio++;
                            }
                        }
                    }
                    i--;
                    break;
                }
                esp++;
                //Si se encuentra 4 espacios seguidos los sustituye por un Tab '\t'
                if (esp == 4){
                    if (arr_limpio[ptr_arrlimpio - 1] == '\n' || arr_limpio[ptr_arrlimpio - 1] == '\t'){
                        arr_limpio[ptr_arrlimpio] = '\t';
                        ptr_arrlimpio++;
                    }
                    else if(BuscarSignos(arr_archivo[i], arr_limpio[ptr_arrlimpio -1])){
                        arr_limpio[ptr_arrlimpio] = ' ';
                        ptr_arrlimpio++;
                    }
                    esp = 0;
                }
                
            }
            
        }
        else if(arr_archivo[i] == '/'){
            if(arr_archivo[i + 1] == '/'){
                while (true){
                    i++;
                    if (arr_archivo[i] == '\n'){
                        //Para eliminar la fila si el comentario ocupa una linea completa
                        //if (arr_limpio[ptr_arrlimpio -1] == ';' || arr_limpio[ptr_arrlimpio -1] == '{' || arr_limpio[ptr_arrlimpio -1] == '}' || arr_limpio[ptr_arrlimpio -1] == ')')
                        //{
                            arr_limpio[ptr_arrlimpio] = arr_archivo[i];
                            ptr_arrlimpio++;
                        //}
                        break;
                    }
                }
            }
            else{
                arr_limpio[ptr_arrlimpio] = arr_archivo[i];
                ptr_arrlimpio++;
            }
            
        }
        else if(arr_archivo[i] == '\n'){
            arr_limpio[ptr_arrlimpio] = arr_archivo[i];
            //arr_limpio[ptr_arrlimpio + 1] = '\t';
            ptr_arrlimpio ++;
        }
        else if(arr_archivo[i] == '\"'){
            arr_limpio[ptr_arrlimpio] = arr_archivo[i];
            ptr_arrlimpio++;
            i++;
            while (true)
            {
                if(arr_archivo[i] == '\"'){
                    arr_limpio[ptr_arrlimpio] = arr_archivo[i];
                    ptr_arrlimpio++;
                    break;
                }
                
                arr_limpio[ptr_arrlimpio] = arr_archivo[i];
                ptr_arrlimpio++;
                i++;
            }
        }
        else if(arr_archivo[i] == '\''){
            arr_limpio[ptr_arrlimpio] = arr_archivo[i];
            ptr_arrlimpio++;
            i++;
            while (true)
            {
                if(arr_archivo[i] == '\''){
                    arr_limpio[ptr_arrlimpio] = arr_archivo[i];
                    ptr_arrlimpio++;
                    break;
                }
                arr_limpio[ptr_arrlimpio] = arr_archivo[i];
                ptr_arrlimpio++;
                i++;
            }
        }
        else if(arr_archivo[i] == -85){
            break;
        }
        else{
            arr_limpio[ptr_arrlimpio] = arr_archivo[i];
            ptr_arrlimpio++;
        }
    }

    return arr_limpio;
}

//Validar equivalencia de llaves, corchetes, parentesis, comillas simples y comillas dobles. Y punto y coma
bool ValidarSignos(char* arr_limpio){
    char caract[6] = {'{', '}', '[', ']', '(', ')'};
    char comi[2] = {'\"','\''};
    bool error = true;

    int ren = 1, col = 1, ptrpila = 0;
    char* pila = (char*)calloc(500, 1);
    int* arr_ren = (int*)calloc(500, sizeof(int));
    int* arr_col = (int*)calloc(500, sizeof(int));

    //Validar llaves, parentesis y corchetes
    for (int c = 0; c < 6; c+=2){
        for (int x = 0; x < strlen(arr_limpio); x++){
            if (arr_limpio[x] == caract[c]){
                pila[ptrpila] = arr_limpio[x];
                arr_ren[ptrpila] = ren;
                arr_col[ptrpila] = col;

                ptrpila++;
                col++;
            }
            else if(arr_limpio[x] == caract[c + 1]){
                if (ptrpila == 0){
                    printf("Se esperaba: '{' [%d, %d]\n", ren, col);
                }
                else{
                    ptrpila--;
                    pila[ptrpila] = 0;
                    arr_ren[ptrpila] = 0;
                    arr_col[ptrpila] = 0;
                }
            }
            else if(arr_limpio[x] == '\n'){
                ren++;
                col = 1;
            }
            else if(arr_limpio[x] == '\"'){
                while(true){
                    x++;
                    if (arr_limpio[x] == '\"'){
                        break;
                    }
                }
            }
            else if(arr_limpio[x] == '\''){
                while(true){
                    x++;
                    if (arr_limpio[x] == '\''){
                        break;
                    }
                }
            }
            else{
                if(arr_limpio[x] != '\t'){
                    col++;
                }
            }
        }
        for (int i = strlen(pila) - 1; i >= 0; i--){
            if (pila[i] == '{'){
                printf("Se esperaba: '}' [%d, %d]\n", arr_ren[i], arr_col[i]);
            }
            else if(pila[i] == '}'){
                printf("Se esperaba: '{' [%d, %d]\n", arr_ren[i], arr_col[i]);
            }
            else if(pila[i] == '('){
                printf("Se esperaba: ')' [%d, %d]\n", arr_ren[i], arr_col[i]);
            }
            else if(pila[i] == ')'){
                printf("Se esperaba: '(' [%d, %d]\n", arr_ren[i], arr_col[i]);
            }
            else if(pila[i] == '['){
                printf("Se esperaba: ']' [%d, %d]\n", arr_ren[i], arr_col[i]);
            }
            else if(pila[i] == ']'){
                printf("Se esperaba: '[' [%d, %d]\n", arr_ren[i], arr_col[i]);
            }
            pila[i] = 0;
            arr_ren[i] = 0;
            arr_col[i] = 0;

            if(i == 0) error = false;
        }
        ren = 1; col = 1; ptrpila = 0;
    }
    //Validar comillas, simples y dobles
    for (int c = 0; c < 2; c++){
        for (int x = 0; x < strlen(arr_limpio); x++){
            if (arr_limpio[x] == comi[c]){
                pila[ptrpila] = arr_limpio[x];
                arr_ren[ptrpila] = ren;
                arr_col[ptrpila] = col;

                ptrpila++;
                col++;
                while(true){
                    x++;
                    if(arr_limpio[x] == comi[c]){
                        ptrpila--;
                        pila[ptrpila] = 0;
                        arr_ren[ptrpila] = 0;
                        arr_col[ptrpila] = 0;

                        break;
                    }
                    else if(x == strlen(arr_limpio)){
                        break;
                    }
                }
            }
        }

        for (int i = strlen(pila) - 1; i >= 0; i--){
            printf("Se esperaba: %c [%d, %d]\n", pila[i], arr_ren[i], arr_col[i]);
            pila[i] = 0;
            arr_ren[i] = 0;
            arr_col[i] = 0;
            if(i == 0) error = false;
        }
        ren = 1; col = 1;
    }
    //Validar punto y coma
    for (int x = 0; x < strlen(arr_limpio); x++){
        if (arr_limpio[x] == '\n'){
            if(col > 1){
                if(arr_limpio[x-1] != '{' && arr_limpio[x-1] != '}' && arr_limpio[x-1] != ';' && arr_limpio[x-1] != '\t'){
                    printf("Se esperaba: ';' [%d, %d]\n", ren, col);
                    error = false;
                }
            }
            ren++;
            col = 1;
        }
        else{
            col++;
        }
        
    }
    return error;
}

//Validar declaración de variables y funiones
//Validar si es el nombre de una variable
bool ValidarNomVar(){
    fgets(caract, 2, archivo);
    if (!isalpha(caract[0])){
        fseek(archivo, aux, SEEK_SET);
        return false;
    }
    else{
        fseek(archivo, aux, SEEK_SET);
    }
    for (int i = 0; (!feof(archivo)); i++)
    {
        fgets(caract, 2, archivo);
        if (!isalnum(caract[0]) | caract[0] == 95){
            aux = ftell(archivo) - 1;
            fseek(archivo, aux, SEEK_SET);
            return true;
        }else{
            //guardar letra
            dato[i] = caract[0];
        }
    }
    //si se termina el archivo sin encontrar un espacio, punto y coma o =
    fseek(archivo, aux, SEEK_SET);
    return false;
}

//Validar si se trata de un texto
bool ValidarTexto(){
    fseek(archivo, aux-1, SEEK_SET);
    fgets(caract, 2, archivo);
    if (caract[0] != t_comillad[0]){
        fseek(archivo, aux, SEEK_SET);
        return false;
    }
    for (int i = 0; (!feof(archivo)); i++)
    {
        fgets(caract, 2, archivo);
        if (caract[0] == t_comillad[0]){
            aux = ftell(archivo) - 1;
            fseek(archivo, aux, SEEK_SET);
            return true;
        }
        //Ir guardando token de texto
        dato[i] = caract[0];
    }
    return false;
}

//Validar si se trata de un numero tipo double
bool ValidarNumDouble(){
    int pun = 0;
    fgets(caract, 2, archivo);
    if (isdigit(caract[0])){
        //Guardar número
        dato[0] = caract[0];
        for (int i = 0; (!feof(archivo)); i++){
            if (pun > 1)
            {
                fseek(archivo, aux, SEEK_SET);
                return false;
            }
            
            fgets(caract, 2, archivo);
            if (caract[0] == t_fin[0] | caract[0] == t_cierra_pare[0]){
                aux = ftell(archivo) - 1;
                fseek(archivo, aux, SEEK_SET);
                return true;
            }
            else{
                if (isdigit(caract[0])){
                    //Guardar número
                    dato[i+1] = caract[0];
                }
                else if(caract[0] == 46){
                    dato[i+1] = caract[0];
                    pun++;
                }
                else{
                    fseek(archivo, aux, SEEK_SET);
                    return false;
                }
            }
        }
    }
    else{
        fseek(archivo, aux, SEEK_SET);
        return false;
    }
} 

void SaveVarNFunc(){
    ptr_token = 0;
    for (ptr_limpio = 0; ptr_limpio < strlen(arr_limpio); ptr_limpio++){
        if(IsToken(t_int[2])){
            strcpy(arr_tokens[ptr_token][0], t_int[0]);
            strcpy(arr_tokens[ptr_token][1], "Entero");
            strcpy(arr_tokens[ptr_token][2], t_int[2]);
            ptr_token++;
        }
        else if(IsToken(t_float[2])){
            strcpy(arr_tokens[ptr_token][0], t_float[0]);
            strcpy(arr_tokens[ptr_token][1], "Decimal");
            strcpy(arr_tokens[ptr_token][2], t_float[2]);
            ptr_token++;
        }
        else if(IsToken(t_char[2])){
            strcpy(arr_tokens[ptr_token][0], t_char[0]);
            strcpy(arr_tokens[ptr_token][1], "String");
            strcpy(arr_tokens[ptr_token][2], t_char[2]);
            ptr_token++;
        }
        else if(IsToken(t_bool[2])){
            strcpy(arr_tokens[ptr_token][0], t_bool[0]);
            strcpy(arr_tokens[ptr_token][1], "Booleano");
            strcpy(arr_tokens[ptr_token][2], t_bool[2]);
            ptr_token++;
        }
        else if(IsToken(t_const[2])){
            strcpy(arr_tokens[ptr_token][0], t_const[0]);
            strcpy(arr_tokens[ptr_token][1], "Constante");
            strcpy(arr_tokens[ptr_token][2], t_const[2]);
            ptr_token++;
        }
    }
}

//Analiza si es un token válido
bool IsToken(char token[10]){
    if (token == ""){
        if (arr_tokens[ptr_token -1] == " " && (arr_tokens[ptr_token -2] == t_int[1] || arr_tokens[ptr_token -2] == t_float || arr_tokens[ptr_token -2] == t_float || arr_tokens[ptr_token -2] == t_float || arr_tokens[ptr_token -2] == t_float))
        {
            
        }
    }
    else{
        int save = ptr_limpio;
        for (int i = 0; i < strlen(arr_limpio); i++){
            if(token[i] == '\0'){
                return true;
            }
            else if(arr_limpio[ptr_limpio] != token[i]){
                ptr_limpio = save;
                return false;
            }
            ptr_limpio++;
        }
    }
}

//Reconocer tokens
void Tokenizador(char* arr_limpio){
    int ren = 1;
    ptr_token = 0;

    for (ptr_limpio = 0; ptr_limpio < strlen(arr_limpio); ptr_limpio++){
        if(IsToken(t_int[2])){
            strcpy(arr_tokens[ptr_token][0], t_int[0]);
            strcpy(arr_tokens[ptr_token][1], "Entero");
            strcpy(arr_tokens[ptr_token][2], t_int[2]);
            ptr_token++;
        }
        else if(IsToken(t_float[2])){
            strcpy(arr_tokens[ptr_token][0], t_float[0]);
            strcpy(arr_tokens[ptr_token][1], "Decimal");
            strcpy(arr_tokens[ptr_token][2], t_float[2]);
            ptr_token++;
        }
        else if(IsToken(t_char[2])){
            strcpy(arr_tokens[ptr_token][0], t_char[0]);
            strcpy(arr_tokens[ptr_token][1], "String");
            strcpy(arr_tokens[ptr_token][2], t_char[2]);
            ptr_token++;
        }
        else if(IsToken(t_bool[2])){
            strcpy(arr_tokens[ptr_token][0], t_bool[0]);
            strcpy(arr_tokens[ptr_token][1], "Booleano");
            strcpy(arr_tokens[ptr_token][2], t_bool[2]);
            ptr_token++;
        }
        else if(IsToken(t_const[2])){
            strcpy(arr_tokens[ptr_token][0], t_const[0]);
            strcpy(arr_tokens[ptr_token][1], "Constante");
            strcpy(arr_tokens[ptr_token][2], t_const[2]);
            ptr_token++;
        }
        else if(IsToken(t_printf[2])){
            strcpy(arr_tokens[ptr_token][0], t_printf[0]);
            strcpy(arr_tokens[ptr_token][1], "Imprimir");
            strcpy(arr_tokens[ptr_token][2], t_printf[2]);
            ptr_token++;
        }
        else if(IsToken(t_scanf[2])){
            strcpy(arr_tokens[ptr_token][0], t_scanf[0]);
            strcpy(arr_tokens[ptr_token][1], "Entrada");
            strcpy(arr_tokens[ptr_token][2], t_scanf[2]);
            ptr_token++;
        }
        else if(IsToken(t_if[2])){
            strcpy(arr_tokens[ptr_token][0], t_if[0]);
            strcpy(arr_tokens[ptr_token][1], "If");
            strcpy(arr_tokens[ptr_token][2], t_if[2]);
            ptr_token++;
        }
        else if(IsToken(t_else[2])){
            strcpy(arr_tokens[ptr_token][0], t_else[0]);
            strcpy(arr_tokens[ptr_token][1], "Else");
            strcpy(arr_tokens[ptr_token][2], t_else[2]);
            ptr_token++;
        }
        else if(IsToken(t_for[2])){
            strcpy(arr_tokens[ptr_token][0], t_for[0]);
            strcpy(arr_tokens[ptr_token][1], "For");
            strcpy(arr_tokens[ptr_token][2], t_for[2]);
            ptr_token++;
        }
        else if(IsToken(t_while[2])){
            strcpy(arr_tokens[ptr_token][0], t_while[0]);
            strcpy(arr_tokens[ptr_token][1], "While");
            strcpy(arr_tokens[ptr_token][2], t_while[2]);
            ptr_token++;
        }
        else if(IsToken(t_true[2])){
            strcpy(arr_tokens[ptr_token][0], t_true[0]);
            strcpy(arr_tokens[ptr_token][1], "True");
            strcpy(arr_tokens[ptr_token][2], t_true[2]);
            ptr_token++;
        }
        else if(IsToken(t_false[2])){
            strcpy(arr_tokens[ptr_token][0], t_false[0]);
            strcpy(arr_tokens[ptr_token][1], "False");
            strcpy(arr_tokens[ptr_token][2], t_false[2]);
            ptr_token++;
        }
        else if(IsToken(t_break[2])){
            strcpy(arr_tokens[ptr_token][0], t_break[0]);
            strcpy(arr_tokens[ptr_token][1], "Break");
            strcpy(arr_tokens[ptr_token][2], t_break[2]);
            ptr_token++;
        }
        else if(IsToken(t_return[2])){
            strcpy(arr_tokens[ptr_token][0], t_return[0]);
            strcpy(arr_tokens[ptr_token][1], "Return");
            strcpy(arr_tokens[ptr_token][2], t_return[2]);
            ptr_token++;
        }
        else if(IsToken(t_include[2])){
            strcpy(arr_tokens[ptr_token][0], t_include[0]);
            strcpy(arr_tokens[ptr_token][1], "Require");
            strcpy(arr_tokens[ptr_token][2], t_include[2]);
            ptr_token++;
        }
        else if(IsToken(t_abre_pare[2])){
            strcpy(arr_tokens[ptr_token][0], t_abre_pare[0]);
            strcpy(arr_tokens[ptr_token][1], "Abre parentesis");
            strcpy(arr_tokens[ptr_token][2], t_abre_pare[2]);
            ptr_token++;
        }
        else if(IsToken(t_cierra_pare[2])){
            strcpy(arr_tokens[ptr_token][0], t_cierra_pare[0]);
            strcpy(arr_tokens[ptr_token][1], "Cierra parentesis");
            strcpy(arr_tokens[ptr_token][2], t_cierra_pare[2]);
            ptr_token++;
        }
        else if(IsToken(t_abre_llave[2])){
            strcpy(arr_tokens[ptr_token][0], t_abre_llave[0]);
            strcpy(arr_tokens[ptr_token][1], "Abre llave");
            strcpy(arr_tokens[ptr_token][2], t_abre_llave[2]);
            ptr_token++;
        }
        else if(IsToken(t_cierra_llave[2])){
            strcpy(arr_tokens[ptr_token][0], t_cierra_llave[0]);
            strcpy(arr_tokens[ptr_token][1], "Cierra llave");
            strcpy(arr_tokens[ptr_token][2], t_cierra_llave[2]);
            ptr_token++;
        }
        else if(IsToken(t_abre_corche[2])){
            strcpy(arr_tokens[ptr_token][0], t_abre_corche[0]);
            strcpy(arr_tokens[ptr_token][1], "Abre corchete");
            strcpy(arr_tokens[ptr_token][2], t_abre_corche[2]);
            ptr_token++;
        }
        else if(IsToken(t_cierra_corche[2])){
            strcpy(arr_tokens[ptr_token][0], t_cierra_corche[0]);
            strcpy(arr_tokens[ptr_token][1], "Cierra corchete");
            strcpy(arr_tokens[ptr_token][2], t_cierra_corche[2]);
            ptr_token++;
        }
        else if(IsToken(t_suma[2])){
            strcpy(arr_tokens[ptr_token][0], t_suma[0]);
            strcpy(arr_tokens[ptr_token][1], "Signo Suma");
            strcpy(arr_tokens[ptr_token][2], t_suma[2]);
            ptr_token++;
        }
        else if(IsToken(t_resta[2])){
            strcpy(arr_tokens[ptr_token][0], t_resta[0]);
            strcpy(arr_tokens[ptr_token][1], "Signo Resta");
            strcpy(arr_tokens[ptr_token][2], t_resta[2]);
            ptr_token++;
        }
        else if(IsToken(t_multi[2])){
            strcpy(arr_tokens[ptr_token][0], t_multi[0]);
            strcpy(arr_tokens[ptr_token][1], "Signo Multi");
            strcpy(arr_tokens[ptr_token][2], t_multi[2]);
            ptr_token++;
        }
        else if(IsToken(t_divi[2])){
            strcpy(arr_tokens[ptr_token][0], t_divi[0]);
            strcpy(arr_tokens[ptr_token][1], "Signo Divi");
            strcpy(arr_tokens[ptr_token][2], t_divi[2]);
            ptr_token++;
        }
        else if(IsToken(t_menorque[2])){
            strcpy(arr_tokens[ptr_token][0], t_menorque[0]);
            strcpy(arr_tokens[ptr_token][1], "Menor Que");
            strcpy(arr_tokens[ptr_token][2], t_menorque[2]);
            ptr_token++;
        }
        else if(IsToken(t_mayorque[2])){
            strcpy(arr_tokens[ptr_token][0], t_mayorque[0]);
            strcpy(arr_tokens[ptr_token][1], "Mayor Que");
            strcpy(arr_tokens[ptr_token][2], t_mayorque[2]);
            ptr_token++;
        }
        else if(IsToken(t_asignacion[2])){
            strcpy(arr_tokens[ptr_token][0], t_asignacion[0]);
            strcpy(arr_tokens[ptr_token][1], "Asignacion");
            strcpy(arr_tokens[ptr_token][2], t_asignacion[2]);
            ptr_token++;
        }
        else if(IsToken(t_and[2])){
            strcpy(arr_tokens[ptr_token][0], t_and[0]);
            strcpy(arr_tokens[ptr_token][1], "Ope AND");
            strcpy(arr_tokens[ptr_token][2], t_and[2]);
            ptr_token++;
        }
        else if(IsToken(t_or[2])){
            strcpy(arr_tokens[ptr_token][0], t_or[0]);
            strcpy(arr_tokens[ptr_token][1], "Ope OR");
            strcpy(arr_tokens[ptr_token][2], t_or[2]);
            ptr_token++;
        }
        else if(IsToken(t_nega[2])){
            strcpy(arr_tokens[ptr_token][0], t_nega[0]);
            strcpy(arr_tokens[ptr_token][1], "Ope Nega");
            strcpy(arr_tokens[ptr_token][2], t_nega[2]);
            ptr_token++;
        }
        else if(IsToken(t_espacio[2])){
            strcpy(arr_tokens[ptr_token][0], t_espacio[0]);
            strcpy(arr_tokens[ptr_token][1], "Espacio");
            strcpy(arr_tokens[ptr_token][2], t_espacio[2]);
            ptr_token++;
        }
        else if(IsToken(t_comillad[2])){
            strcpy(arr_tokens[ptr_token][0], t_comillad[0]);
            strcpy(arr_tokens[ptr_token][1], "Comilla Doble");
            strcpy(arr_tokens[ptr_token][2], t_comillad[2]);
            ptr_token++;
        }
        else if(IsToken(t_comillas[2])){
            strcpy(arr_tokens[ptr_token][0], t_comillas[0]);
            strcpy(arr_tokens[ptr_token][1], "Comilla Simple");
            strcpy(arr_tokens[ptr_token][2], t_comillas[2]);
            ptr_token++;
        }
        else if(IsToken(t_salto[2])){
            strcpy(arr_tokens[ptr_token][0], t_salto[0]);
            strcpy(arr_tokens[ptr_token][1], "Salto Linea");
            strcpy(arr_tokens[ptr_token][2], t_salto[2]);
            ptr_token++;
        }
        else if(IsToken(t_tabu[2])){
            strcpy(arr_tokens[ptr_token][0], t_tabu[0]);
            strcpy(arr_tokens[ptr_token][1], "Tabulacion");
            strcpy(arr_tokens[ptr_token][2], t_tabu[2]);
            ptr_token++;
        }
        else if(IsToken(t_pycoma[2])){
            strcpy(arr_tokens[ptr_token][0], t_pycoma[0]);
            strcpy(arr_tokens[ptr_token][1], "Punto y Coma");
            strcpy(arr_tokens[ptr_token][2], t_pycoma[2]);
            ptr_token++;
        }
        else if(IsToken("")){
            strcpy(arr_tokens[ptr_token][0], t_int[0]);
            strcpy(arr_tokens[ptr_token][1], "Punto y Coma");
            strcpy(arr_tokens[ptr_token][2], t_pycoma[2]);
            ptr_token++;
        }
    }
}
int main(int argc, char const *argv[]){
    archivo = fopen("Compilador ALV/cod_ejem.txt", "r");
    //Saco el tamaño del archivo
    fseek(archivo, 0L, SEEK_END);
    int filesize = (ftell(archivo));
    fseek(archivo, 0L, SEEK_SET);

    //En arr_archivo se guardará todo el archivo
    char *arr_archivo = (char*)calloc(filesize, sizeof(char));
    //char arr_archivo[filesize];

    //En arr_limpio se guardará el archivo sin comentarios, Tabs, mas de un espacio y así
    arr_limpio = (char*)calloc(filesize, sizeof(char));
    //char arr_limpio[filesize];

    if(archivo == NULL){
        printf("\nError al abrir el archivo.  :c\n\n");
    }
    else{
        //Copio el archivo en un vector
        char caract;//Aqui se guarda cada caracter

        for(int cont = 0; !feof(archivo) ; cont++){
            caract = fgetc(archivo);
            if (caract != -1){
                arr_archivo[cont] = caract;
            } 
        }
        fclose(archivo);

        //Limpiamos el codigo de un espacio o mas, comentarios, tabulaciones y así
        LimpiarCodigo(arr_archivo, arr_limpio);
        //Limpiar basura en arr_limpio
        for (int i = 0; i < strlen(arr_limpio); i++){
            if (arr_limpio[i] == -85){
                arr_limpio[i] = '\0';
            }
        }

        Tokenizador(arr_limpio);
        for (int i = 0; arr_tokens[i][0][0] != '\0' ; i++)
        {
            printf("%s:\t%s\n", arr_tokens[i][0], arr_tokens[i][1]);
        }
        /*if (ValidarSignos(arr_limpio)){
            archivo = fopen("limpio.txt", "w");
            fputs(arr_limpio, archivo);
            fclose(archivo);

            archivo = fopen("limpio.txt", "r");
            Tokenizador2(arr_limpio);
            fclose(archivo);

            remove("limpio.txt");
        }*/
    }

    printf("\n\n");
    system("pause");
    return 0;
}