#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <stdbool.h>

//Header y Footer del archivo
char header[] = "#include <stdio.h>\n#include <stdlib.h>\n#include <ctype.h>\n#include <string.h>\n#include <stdbool.h>\n\nint main(int argc, char const *argv[])\n{\n\t";
char footer[] = "\n\n\tprintf(\"\\n\");\n\tsystem(\"pause\");\n\treturn 0;\n}";
char **arr_variables;
char **arr_funciones;
FILE *archivo;
char caract[2];
char dato[100];
int aux;
char arr_token[500][2][100];

//Tokens :D
//Palabras reservadas: C | Lenguaje propio
char t_int[2][4] = {"int", "ent"};
char t_double[2][10] = {"float", "decimal"};
char t_string[2][10] = {"char[]", "string"};
char t_boolean[2][10] = {"bool", "bool"};
char t_printf[2][10] = {"printf","show"};
char t_scanf[2][10] = {"scanf","input"};
char t_if[2][10] = {"if","if"};
char t_else[2][10] = {"else","else"};
char t_for[2][10] = {"for","for"};
char t_while[2][10] = {"while","while"};
char t_verdadero[2][10] = {"true","true"};
char t_falso[2][10] = {"false","false"};
char t_break[2][10] = {"break","break"};
char t_return[2][10] = {"return","return"};
char t_return[2][10] = {"#include","require"};

//Signos de agrupación
char t_abre_pare[] = "(";
char t_cierra_pare[] = ")";
char t_abre_llave[] = "{";
char t_cierra_llave[] = "}";
char t_abre_corche[] = "[";
char t_cierra_corche[] = "]";

//Operadores aritméticos
char t_suma[] = "+";
char t_resta[] = "-";
char t_multi[] = "*";
char t_divi[] = "/";

//Operadores relacionales
char t_menorque[] = "<";
char t_mayorque[] = ">";
char t_asignacion[] = "=";

//Operadores lógicos
char t_and[2][5] = {"&&","and"};
char t_or[2][5] = {"||","or"};
char t_nega[] = "!";

//Demás
char t_espacio[] = " ";
char t_comillad[] = "\"";
char t_comillas[] = "\'";
char t_salto[] = "\n";
char t_tabu[] = "\t";
char t_fin[] = ";";


//Funcion que busca si el caracter actual o el ultimo caracter de arr_limpio es uno de estos signos
bool BuscarSignos(char actu, char ante){ //Caracter actual de arr_archivo y ultimo de arr_limpio
    char signos[] = {
        '(',')',
        '{','}',
        '[',']',
        ';','=',
        '+','-',
        '*','/',
        '<','>',
        ','
    };

    //Si encuentra una coincidencia regresa false
    for(int x = 0; x < strlen(signos); x++){
        if(actu == signos[x] || ante == signos[x]){
            return false;
        }
    }
    return true;
}

//Quitar comentarios, tabulaciones, espacios dobles y más
char* LimpiarCodigo(char* arr_archivo, char* arr_limpio){
    //Recorro todo el archivo guardado
    int ptr_arrlimpio = 0; //Puntero de arr_limpio

    for (int i = 0; i < strlen(arr_archivo); i++){//La i es el puntero del archivo guardado (arr_archivo)
        if (arr_archivo[i] == ' '){
            int esp = 1;
            
            while (true){
                i++;
                if (arr_archivo[i] != ' '){
                    //Si el caracter actual o el último de arr_limpio es uno de los signos que
                    //busca la función BuscarSignos entonces no pone el espacio
                    if (BuscarSignos(arr_archivo[i], arr_limpio[ptr_arrlimpio -1])){
                        if(esp > 0) {
                            if(arr_limpio[ptr_arrlimpio - 1] != ' '){
                                arr_limpio[ptr_arrlimpio] = ' ';
                                ptr_arrlimpio++;
                            }
                        }
                    }
                    i--;
                    break;
                }
                esp++;
                //Si se encuentra 4 espacios seguidos los sustituye por un Tab '\t'
                if (esp == 4){
                    if (arr_limpio[ptr_arrlimpio - 1] == '\n' || arr_limpio[ptr_arrlimpio - 1] == '\t'){
                        arr_limpio[ptr_arrlimpio] = '\t';
                        ptr_arrlimpio++;
                    }
                    else if(BuscarSignos(arr_archivo[i], arr_limpio[ptr_arrlimpio -1])){
                        arr_limpio[ptr_arrlimpio] = ' ';
                        ptr_arrlimpio++;
                    }
                    esp = 0;
                }
                
            }
            
        }
        else if(arr_archivo[i] == '/'){
            if(arr_archivo[i + 1] == '/'){
                while (true){
                    i++;
                    if (arr_archivo[i] == '\n'){
                        //Para eliminar la fila si el comentario ocupa una linea completa
                        //if (arr_limpio[ptr_arrlimpio -1] == ';' || arr_limpio[ptr_arrlimpio -1] == '{' || arr_limpio[ptr_arrlimpio -1] == '}' || arr_limpio[ptr_arrlimpio -1] == ')')
                        //{
                            arr_limpio[ptr_arrlimpio] = arr_archivo[i];
                            ptr_arrlimpio++;
                        //}
                        break;
                    }
                }
            }
            else{
                arr_limpio[ptr_arrlimpio] = arr_archivo[i];
                ptr_arrlimpio++;
            }
            
        }
        else if(arr_archivo[i] == '\n'){
            arr_limpio[ptr_arrlimpio] = arr_archivo[i];
            //arr_limpio[ptr_arrlimpio + 1] = '\t';
            ptr_arrlimpio ++;
        }
        else if(arr_archivo[i] == '\"'){
            arr_limpio[ptr_arrlimpio] = arr_archivo[i];
            ptr_arrlimpio++;
            i++;
            while (true)
            {
                if(arr_archivo[i] == '\"'){
                    arr_limpio[ptr_arrlimpio] = arr_archivo[i];
                    ptr_arrlimpio++;
                    break;
                }
                
                arr_limpio[ptr_arrlimpio] = arr_archivo[i];
                ptr_arrlimpio++;
                i++;
            }
        }
        else if(arr_archivo[i] == '\''){
            arr_limpio[ptr_arrlimpio] = arr_archivo[i];
            ptr_arrlimpio++;
            i++;
            while (true)
            {
                if(arr_archivo[i] == '\''){
                    arr_limpio[ptr_arrlimpio] = arr_archivo[i];
                    ptr_arrlimpio++;
                    break;
                }
                arr_limpio[ptr_arrlimpio] = arr_archivo[i];
                ptr_arrlimpio++;
                i++;
            }
        }
        else if(arr_archivo[i] == -85){
            break;
        }
        else{
            arr_limpio[ptr_arrlimpio] = arr_archivo[i];
            ptr_arrlimpio++;
        }
    }

    return arr_limpio;
}

//Validar equivalencia de llaves, corchetes, parentesis, comillas simples y comillas dobles. Y punto y coma
bool ValidarSignos(char* arr_limpio){
    char caract[6] = {'{', '}', '[', ']', '(', ')'};
    char comi[2] = {'\"','\''};
    bool error = true;

    int ren = 1, col = 1, ptrpila = 0;
    char* pila = (char*)calloc(500, 1);
    int* arr_ren = (int*)calloc(500, sizeof(int));
    int* arr_col = (int*)calloc(500, sizeof(int));

    //Validar llaves, parentesis y corchetes
    for (int c = 0; c < 6; c+=2){
        for (int x = 0; x < strlen(arr_limpio); x++){
            if (arr_limpio[x] == caract[c]){
                pila[ptrpila] = arr_limpio[x];
                arr_ren[ptrpila] = ren;
                arr_col[ptrpila] = col;

                ptrpila++;
                col++;
            }
            else if(arr_limpio[x] == caract[c + 1]){
                if (ptrpila == 0){
                    printf("Se esperaba: '{' [%d, %d]\n", ren, col);
                }
                else{
                    ptrpila--;
                    pila[ptrpila] = 0;
                    arr_ren[ptrpila] = 0;
                    arr_col[ptrpila] = 0;
                }
            }
            else if(arr_limpio[x] == '\n'){
                ren++;
                col = 1;
            }
            else if(arr_limpio[x] == '\"'){
                while(true){
                    x++;
                    if (arr_limpio[x] == '\"'){
                        break;
                    }
                }
            }
            else if(arr_limpio[x] == '\''){
                while(true){
                    x++;
                    if (arr_limpio[x] == '\''){
                        break;
                    }
                }
            }
            else{
                if(arr_limpio[x] != '\t'){
                    col++;
                }
            }
        }
        for (int i = strlen(pila) - 1; i >= 0; i--){
            if (pila[i] == '{'){
                printf("Se esperaba: '}' [%d, %d]\n", arr_ren[i], arr_col[i]);
            }
            else if(pila[i] == '}'){
                printf("Se esperaba: '{' [%d, %d]\n", arr_ren[i], arr_col[i]);
            }
            else if(pila[i] == '('){
                printf("Se esperaba: ')' [%d, %d]\n", arr_ren[i], arr_col[i]);
            }
            else if(pila[i] == ')'){
                printf("Se esperaba: '(' [%d, %d]\n", arr_ren[i], arr_col[i]);
            }
            else if(pila[i] == '['){
                printf("Se esperaba: ']' [%d, %d]\n", arr_ren[i], arr_col[i]);
            }
            else if(pila[i] == ']'){
                printf("Se esperaba: '[' [%d, %d]\n", arr_ren[i], arr_col[i]);
            }
            pila[i] = 0;
            arr_ren[i] = 0;
            arr_col[i] = 0;

            if(i == 0) error = false;
        }
        ren = 1; col = 1; ptrpila = 0;
    }
    //Validar comillas, simples y dobles
    for (int c = 0; c < 2; c++){
        for (int x = 0; x < strlen(arr_limpio); x++){
            if (arr_limpio[x] == comi[c]){
                pila[ptrpila] = arr_limpio[x];
                arr_ren[ptrpila] = ren;
                arr_col[ptrpila] = col;

                ptrpila++;
                col++;
                while(true){
                    x++;
                    if(arr_limpio[x] == comi[c]){
                        ptrpila--;
                        pila[ptrpila] = 0;
                        arr_ren[ptrpila] = 0;
                        arr_col[ptrpila] = 0;

                        break;
                    }
                    else if(x == strlen(arr_limpio)){
                        break;
                    }
                }
            }
        }

        for (int i = strlen(pila) - 1; i >= 0; i--){
            printf("Se esperaba: %c [%d, %d]\n", pila[i], arr_ren[i], arr_col[i]);
            pila[i] = 0;
            arr_ren[i] = 0;
            arr_col[i] = 0;
            if(i == 0) error = false;
        }
        ren = 1; col = 1;
    }
    //Validar punto y coma
    for (int x = 0; x < strlen(arr_limpio); x++){
        if (arr_limpio[x] == '\n'){
            if(col > 1){
                if(arr_limpio[x-1] != '{' && arr_limpio[x-1] != '}' && arr_limpio[x-1] != ';' && arr_limpio[x-1] != '\t'){
                    printf("Se esperaba: ';' [%d, %d]\n", ren, col);
                    error = false;
                }
            }
            ren++;
            col = 1;
        }
        else{
            col++;
        }
        
    }
    return error;
}

//Validar declaración de variables y funiones
//Validar si es el nombre de una variable
bool ValidarNomVar(){
    fgets(caract, 2, archivo);
    if (!isalpha(caract[0])){
        fseek(archivo, aux, SEEK_SET);
        return false;
    }
    else{
        fseek(archivo, aux, SEEK_SET);
    }
    for (int i = 0; (!feof(archivo)); i++)
    {
        fgets(caract, 2, archivo);
        if (!isalnum(caract[0]) | caract[0] == 95){
            aux = ftell(archivo) - 1;
            fseek(archivo, aux, SEEK_SET);
            return true;
        }else{
            //guardar letra
            dato[i] = caract[0];
        }
    }
    //si se termina el archivo sin encontrar un espacio, punto y coma o =
    fseek(archivo, aux, SEEK_SET);
    return false;
}

//Validar si se trata de un texto
bool ValidarTexto(){
    fseek(archivo, aux-1, SEEK_SET);
    fgets(caract, 2, archivo);
    if (caract[0] != t_comillad[0]){
        fseek(archivo, aux, SEEK_SET);
        return false;
    }
    for (int i = 0; (!feof(archivo)); i++)
    {
        fgets(caract, 2, archivo);
        if (caract[0] == t_comillad[0]){
            aux = ftell(archivo) - 1;
            fseek(archivo, aux, SEEK_SET);
            return true;
        }
        //Ir guardando token de texto
        dato[i] = caract[0];
    }
    return false;
}

//Validar si se trata de un numero tipo double
bool ValidarNumDouble(){
    int pun = 0;
    fgets(caract, 2, archivo);
    if (isdigit(caract[0])){
        //Guardar número
        dato[0] = caract[0];
        for (int i = 0; (!feof(archivo)); i++){
            if (pun > 1)
            {
                fseek(archivo, aux, SEEK_SET);
                return false;
            }
            
            fgets(caract, 2, archivo);
            if (caract[0] == t_fin[0] | caract[0] == t_cierra_pare[0]){
                aux = ftell(archivo) - 1;
                fseek(archivo, aux, SEEK_SET);
                return true;
            }
            else{
                if (isdigit(caract[0])){
                    //Guardar número
                    dato[i+1] = caract[0];
                }
                else if(caract[0] == 46){
                    dato[i+1] = caract[0];
                    pun++;
                }
                else{
                    fseek(archivo, aux, SEEK_SET);
                    return false;
                }
            }
        }
    }
    else{
        fseek(archivo, aux, SEEK_SET);
        return false;
    }
}

//Analiza si es un token válido
bool IsToken(char dat[10]){
    for (int i=0; (!feof(archivo)); i++ ){
        if(dat[i]=='\0'){
            strcpy(dato, dat);
            aux = ftell(archivo);
            return true;
        }
        fgets(caract, 2, archivo);
        if (caract[0] != dat[i]){
            fseek(archivo, aux, SEEK_SET);
            return false;
        }
    }
    return false;
}

//Reconocer tokens
void Tokenizador(){
    aux = ftell(archivo);
    int i=0;
    int ren = 1;
    char var;
    while (!feof(archivo)){
        if (IsToken(t_break)){
            strcpy(arr_token[i][0], "Break");
            //guardar token y proseguir
        }
        else if (IsToken(t_while)){
            strcpy(arr_token[i][0], "While");
            //guardar token y proseguir
        }
        else if (IsToken(t_double)){
            strcpy(arr_token[i][0], "Double");
            //guardar token y proseguir
        }
        else if (IsToken(t_if)){
            strcpy(arr_token[i][0], "If");
            //guardar token y proseguir
        }
        else if (IsToken(t_else)){
            strcpy(arr_token[i][0], "Else");
            //guardar token y proseguir
        }
        else if (IsToken(t_for)){
            strcpy(arr_token[i][0], "For");
            //guardar token y proseguir
        }
        else if (IsToken(t_int)){
            strcpy(arr_token[i][0], "Int");
            //guardar token y proseguir
        }
        else if (IsToken(t_return)){
            strcpy(arr_token[i][0], "Return");
            //guardar token y proseguir
        }
        else if (IsToken(t_string)){
            strcpy(arr_token[i][0], "String");
            //guardar token y proseguir
        }
        else if (IsToken(t_printf)){
            strcpy(arr_token[i][0], "Printf");
            //guardar token y proseguir
        }
        else if (IsToken(t_scanf)){
            strcpy(arr_token[i][0], "Scanf");
            //guardar token y proseguir
        }
        else if (IsToken(t_boolean)){
            strcpy(arr_token[i][0], "Boolean");
            //guardar token y proseguir
        }
        else if (IsToken(t_salto)){
            strcpy(arr_token[i][0], "Salto");
            ren++;
            //guardar token y proseguir
        }
        else if (IsToken(t_abre_llave)){
            strcpy(arr_token[i][0], "{");
            //guardar token y proseguir
        }
        else if (IsToken(t_cierra_llave)){
            strcpy(arr_token[i][0], "}");
            //guardar token y proseguir
        }
        else if (IsToken(t_espacio)){
            strcpy(arr_token[i][0], "Espacio");
            //guardar token y proseguir
        }
        else if (IsToken(t_asignacion)){
            strcpy(arr_token[i][0], "=");
            //guardar token y proseguir
        }
        else if (IsToken(t_fin)){
            strcpy(arr_token[i][0], ";");
            //guardar token y proseguir
        }
        else if (IsToken(t_abre_pare)){
            strcpy(arr_token[i][0], "(");
            //guardar token y proseguir
        }
        else if (IsToken(t_cierra_pare)){
            strcpy(arr_token[i][0], ")");
            //guardar token y proseguir
        }
        else if (IsToken(t_comillad)){
            strcpy(arr_token[i][0], "Comilla doble");
            //guardar token y proseguir
        }
        else if (IsToken(t_comillas)){
            strcpy(arr_token[i][0], "Comilla simple");
            //guardar token y proseguir
        }
        else if (IsToken(t_nega)){
            strcpy(arr_token[i][0], "!");
            //guardar token y proseguir
        }
        else if (IsToken(t_menorque)){
            strcpy(arr_token[i][0], "<");
            //guardar token y proseguir
        }
        else if (IsToken(t_mayorque)){
            strcpy(arr_token[i][0], ">");
            //guardar token y proseguir
        }
        else if (IsToken(t_and)){
            strcpy(arr_token[i][0], "&");
            //guardar token y proseguir
        }
        else if (IsToken(t_or)){
            strcpy(arr_token[i][0], "|");
            //guardar token y proseguir
        }
        else if (IsToken(t_suma)){
            strcpy(arr_token[i][0], "+");
            //guardar token y proseguir
        }
        else if (IsToken(t_resta)){
            strcpy(arr_token[i][0], "-");
            //guardar token y proseguir
        }
        else if (IsToken(t_verdadero)){
            strcpy(arr_token[i][0], "True");
            //guardar token y proseguir
        }
        else if (IsToken(t_falso)){
            strcpy(arr_token[i][0], "False");
            //guardar token y proseguir
        }
        else if (IsToken(t_tabu)){
            strcpy(arr_token[i][0], "Tab");
            //guardar token y proseguir
        }
        else if (ValidarTexto()){
            strcpy(arr_token[i][0], "Texto");
            //guardar token y proseguir
        }
        else if (ValidarNomVar()){
            strcpy(arr_token[i][0], "Nombre");
            //guardar token y proseguir
        }
        else if (ValidarNumDouble()){
            strcpy(arr_token[i][0], "Numero");
            //guardar token y proseguir
        }
        else{
            printf("Token no valido, linea %d\n", ren);
            break;
        }
        strcpy(arr_token[i][1], dato);
        memset(dato,0,100);
        i++;
    }
    //arr_token[i][0] != '\0' no funciona esta condicion
    for (int i = 0; arr_token[i][0][0] != '\0'; i++)
    {
        printf("%s",arr_token[i][0]);
        printf(": %s\n",arr_token[i][1]);
    }
}

int main(int argc, char const *argv[]){
    archivo = fopen("Proyecto Compilador/cod_ejem.txt", "r");
    //Saco el tamaño del archivo
    fseek(archivo, 0L, SEEK_END);
    int filesize = (ftell(archivo));
    fseek(archivo, 0L, SEEK_SET);

    //En arr_archivo se guardará todo el archivo
    char *arr_archivo = (char*)calloc(filesize, sizeof(char));
    //char arr_archivo[filesize];

    //En arr_limpio se guardará el archivo sin comentarios, Tabs, mas de un espacio y así
    char *arr_limpio = (char*)calloc(filesize, sizeof(char));
    //char arr_limpio[filesize];

    if(archivo == NULL){
        printf("\nError al abrir el archivo.  :c\n\n");
    }
    else{
        //Copio el archivo en un vector
        char caract;//Aqui se guarda cada caracter

        for(int cont = 0; !feof(archivo) ; cont++){
            caract = fgetc(archivo);
            if (caract != -1){
                arr_archivo[cont] = caract;
            } 
        }
        fclose(archivo);

        //Limpiamos el codigo de un espacio o mas, comentarios, tabulaciones y así
        LimpiarCodigo(arr_archivo, arr_limpio);
        //Limpiar basura en arr_limpio
        for (int i = 0; i < strlen(arr_limpio); i++){
            if (arr_limpio[i] == -85){
                arr_limpio[i] = '\0';
            }
        }

        if (ValidarSignos(arr_limpio)){
            archivo = fopen("limpio.txt", "w");
            fputs(arr_limpio, archivo);
            fclose(archivo);

            archivo = fopen("limpio.txt", "r");
            Tokenizador();
            fclose(archivo);

            remove("limpio.txt");
        }

        //ValidarVariables(arr_limpio);

        
    }

    printf("\n\n");
    system("pause");
    return 0;
}