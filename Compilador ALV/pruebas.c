
#include <stdio.h>  //Para operaciones de Entrada y Salida (printf/scanf)
#include <stdlib.h> //Para conversión de tipos de datos
#include <string.h> //Operaciones de cadenas
#include <ctype.h>  //Operaciones básicas con caracteres (isdigit(), isalpha(), isspace())

int main()
{
    char input;
    int res;
    
    char t_int[3][5] = {"Tipo","int", "ent"};

    for (int i = 0; i < (sizeof(t_int)/sizeof(t_int[0])); i++)
    {
        printf("%s\n",t_int[i]);
    }
    
    system("pause");
    return 0;
}
